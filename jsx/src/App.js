import React from 'react';
import './App.css';
import CommentDetail from './CommentDetail';
import faker from 'faker';
import ApprovalCard from './ApprovalCard';

const App = () => {
  return (
    <div className="ui container comments">
      <div className="picture"></div>
      <ApprovalCard><CommentDetail author={faker.name.firstName()} avatar={faker.image.avatar()} comments={faker.random.words()} date="Yesterday at 5:00PM" /></ApprovalCard>
      <ApprovalCard><CommentDetail author={faker.name.firstName()} avatar={faker.image.avatar()} comments={faker.random.words()} date="Yesterday at 10:00PM" /></ApprovalCard>
      <ApprovalCard><CommentDetail author={faker.name.firstName()} avatar={faker.image.avatar()} comments={faker.random.words()} date="Today at 4:45PM" /></ApprovalCard>
      <ApprovalCard><CommentDetail author={faker.name.firstName()} avatar={faker.image.avatar()} comments={faker.random.words()} date="Today at 6:15PM" /></ApprovalCard>
    </div>
  );
}

export default App;
